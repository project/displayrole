# Display role

Easily show users' assigned role or roles on their profile pagesby using this
module.

It makes a list of user roles available for site administrators to choose to
show as one of the display fields available on user profiles.

User roles are therefore also available in the user profile template
(user.html.twig) without having to write a preprocess function in the theme or
a module.

Right now it displays a list of all roles that user has but it can and
probably should be made more configurable.

- For a full description of this module, visit the
  [project page](https://www.drupal.org/project/displayrole)

- To submit bug reports and feature suggestions, or track changes
  [issue queue](https://www.drupal.org/project/issues/displayrole)


## Contents of this file

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to People » Account settings » Manage display and drag "Roles" to where you
would like it to display relative to the order of other items, like "Picture"
and "Member for", available on this form that, as it explains, "lets
administrators configure how fields should be displayed when rendering a user
profile page."


## Maintainers

- [David Valdez (gnuget)](https://www.drupal.org/u/gnuget)
- [Benjamin Melançon (mlncn)](https://www.drupal.org/u/mlncn)
