<?php

/**
 * @file
 * Contains hooks for the Display Role module.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function displayrole_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.displayrole':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Display the role of a user on user view (profile) pages by configuring the <em>Roles</em> field to display on the <a href=":user_fields_display">user profile fields display form</a>.', array(':user_fields_display' => Url::fromRoute('entity.entity_view_display.user.default')->toString())) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function displayrole_entity_extra_field_info() {
  $fields['user']['user']['display']['roles'] = array(
    'label' => t('Roles'),
    'description' => t('User module \'roles\' view element.'),
    'weight' => 5,
  );
  return $fields;
}

/**
 * Implements hook_ENTITY_TYPE_view() for user entities.
 */
function displayrole_user_view(array &$build, UserInterface $account, EntityViewDisplayInterface $display) {
  if ($display->getComponent('roles')) {
    $rids = $account->getRoles();
    $roles = array_intersect_key(user_role_names(), array_flip($rids));
    // All the below is following examples in core, but good grief this not-possible-to-override-with-a-simple-template approach is NOT good.
    $build['roles'] = array(
      '#theme' => 'item_list__roles',
      '#items' => $roles,
      '#title' => t('Roles')
    );
  }
}

/**
 * Prepares variables for username templates.
 *
 * Default template: username.html.twig.
 *
 * Modules that make any changes to variables like 'name' or 'extra' must ensure
 * that the final string is safe.
 *
 * @param array $variables
 *   An associative array containing:
 *   - account: The user account (\Drupal\Core\Session\AccountInterface).
 */
function displayrole_preprocess_username(&$variables) {
  if (\Drupal::config('displayrole.settings')->get('append_role_to_username')) {
    $account = $variables['account'];
    $rids = $account->getRoles();
    $roles = array_intersect(user_role_names(), array_flip($rids));
    // All the below is following examples in core, but good grief this not-possible-to-override-with-a-simple-template approach is NOT good.
    $item_list = array(
      '#theme' => 'item_list',
      '#items' => $roles,
    );
    $roles_rendered = \Drupal::service('renderer')->renderPlain($item_list);

    $variables['extra'] .= ' (' . $roles_rendered . ')';
  }
}
